
# WeeklyTimelog Cli

Terminal client for weeklytimelog.com

WeeklyTimelog is an <b>Automatic Time tracking for Developers</b>, that integrates with terminal, git repositories, popular task managers (Asana, Trello, YouTrack, Jira, etc), and many other developer productivity tools. 

<iframe src="https://www.youtube.com/embed/B4zJyYSvNko" width="500px" height="300px"></iframe>

<a class="btn btn-lg btn-info" href="https://weeklytimelog.com" target="_blank" role="button" style="z-index:200">Open WeeklyTimelog WebSite</a>

## The CLI

It was designed to help developers track their terminal activities such as SSHing into servers, deploying, running tests, documenting maintenance work, etc

You may execute the WT command directly from the command line in manual mode or add the command to shell scripts, in order to automatically records the activity. 

## Installation

1. Install NodeJS and npm <a href="https://www.npmjs.com/package/quickly-copy-file/tutorial" target="_blank"> read more.. </a>

2. Run `sudo npm install -g weeklytimelog-cli`
3. Run `wt --help` 

## USAGE

### Config

This command needs to be executed just once to configure the host. It basically authenticates against the WeeklyTimelog server and stores a temporary token in a local setting file. 

        Pre-Requisite: 
                Please register in weeklytimelog.com first, to obtain the <username> and <password>

        Usage:
                wt config <username> <password>

        Sample:
                wt config myuser 123123

### Timer (Start/Stop)

Tracks time spent in real time. 

        Usage:
                wt start <description>

        Samples:
                wt start "Checking backups, cleaning up disk space"
                wt stop

### Last

Records all the SSH access history logs entries in weeklytimelog as activity reminders. 

        Usage:
                wt last


### Activity

An acitivy reminds the developer of work accomplished during the day. It is not an effective timelog, but instead just a reminder. The developer can later log into WeeklyTimelog to convert these activity reminders into real timelogs. 

        Usage:
                wt activity <description>

        Sample:
                wt activity "coding wt cli" 

### Timelog

Describes the task performed and time spent allocated to a project. Timelogs are visible by project owner for further reporting, cost analysis, share with clients in real time, etc. 

        Usage:
                wt timelog <hours> <description> [-p projectId]
                        projectId is optional. 

        Samples:
                wt timelog 1h30m "server maintenance" 
                wt timelog 1.5 "server maintenance" 


        

## USE CASES

* Manually run last command to record all your ssh access history activites. 

* Update your bash profile to start tracking time when SSHing into server. 
  vim ~/.bashrc and add wt activity "Connecting to server for maintenance"

* Update you deployment and testing script to automatically create activities (reminders)

* SSH into server or from local terminal, use start command to record your activity. 

* Manually issue activity and timelogs commands on demand. 


## Further customization - API

The cli will provide basic commands but further customization can be done from any language thru our API. 

* <a href="https://weeklytimelog.com/apidoc/" target="_blank">API documentation</a>

## Support

* <a href="https://weeklytimelog.zendesk.com/hc/en-us" target="_blank">Support Center</a>
* <a href="https://weeklytimelog.zendesk.com/hc/en-us/requests/new" target="_blank">Support request or ideas</a>
* <a href="https://weeklytimelog.zendesk.com/hc/en-us/requests/new" target="_blank">Request customization</a>


