var os = require('os');
var fs = require('fs');
var jsonfile = require('jsonfile');
var path = require('path');

var WtFiles = {
  
  path: os.homedir() + '/.wt_settings.json',
  //path: path.join(__dirname, './settings.json'),

  getPath() {
    return this.path;
  },
      
  loadSettings: function() {
    //default setting
    var r={"env":"prod","accessToken":"","username":""};
    
    if (fs.existsSync(this.path)) {
      r=jsonfile.readFileSync(this.path);
    }
    return  r; 

  },

  writeSettings: function (s) {
    return jsonfile.writeFileSync(this.path, s);
  },
  
      
};
module.exports = WtFiles;
  