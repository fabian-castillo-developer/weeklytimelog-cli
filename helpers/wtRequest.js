// var request = require('request');

var WtRequest = {

    uri: 'https://api.weeklytimelog.com/api/cli/',
        
    setEnv: function(env) {
        if(env=='dev') {
            this.uri= 'http://localhost:4040/api/cli/';
        }
    },

    
    request: function (token, endpoint, data, callback) {
        //console.log('URL request to: ' + this.uri+endpoint )
        
        var request = require('request');
        // console.log(this.uri + endpoint);
        // console.log("token:" + token);
        // console.log(data);
        request.post({
          url:     this.uri + endpoint ,
          headers: {
            'content-type' : 'application/json',
            'authorization': token
          },
          json: true,
          body:  data

        }, function(error, response, body){
            callback({error, response, body});
        });

    },
    
    getToken: function (username, password, callback) {

        var request = require('request');
        var url=this.uri + "config";
        //console.log(url);

        var options = { method: 'POST',
        url: url,
        headers: { 
            'cache-control': 'no-cache',
            'content-type': 'application/json' 
        },
        body: { "username": username, "password": password },
        json: true };
      
        request(options, function (error, response, body) {
            //if (error) throw new Error(error);
        
            //console.log(body);
            callback({error, response, body});

        });
    }

    
};
module.exports = WtRequest;
