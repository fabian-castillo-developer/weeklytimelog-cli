#!/usr/bin/env node 
//--harmony

// AWESOME article that explains how to build a terminal by Tim Pettersen
// https://developer.atlassian.com/blog/2015/11/scripting-with-node/ 


var program = require('commander');
var co = require('co');
var prompt = require('co-prompt');
// var request = require('superagent');
var chalk = require('chalk');
var ProgressBar = require('progress');
var RSVP = require('rsvp');
var wtFiles = require('./helpers/wtFiles.js');
var wtRequest = require('./helpers/wtRequest.js');
var os = require('os');


//loading settings
var settings = wtFiles.loadSettings();
var pjson = require('./package.json');

//sets env (prod by default)
wtRequest.setEnv(settings.env);

//starting client execution
console.log("WT Client v." + pjson.version + " executed by " + settings.username)
console.log( new Date);

function checkAccessToken() {
  //validating accessToken is set
  if(!settings.accessToken) {
    console.log(". you must confing the cli once ");
    console.log("  usage: wt config <username> <password> ");
    process.exit(0);
  } 
}

// console.log(process.argv);

//by default PROD
// program
//   .command('env ')
//   .description('run env if wish to change to dev mode')
//   .option("-s, --setup_mode [mode]", "Which setup mode to use")
//   .action(function(env){
//     var mode = options.setup_mode || "normal";
//     env = env || 'all';
//     console.log('setup for %s env(s) with %s mode', env, mode);
//   });

program
  .command('status')
  .description('Displays username and timer status')
  .action(function() {
    console.log(". Username: " + settings.username);
    console.log(". Timer: " + (settings.timelogStartId ? "ON" : "OFF") );

  });

program
  .command('config <username> <password>')
  .description('Authenticates CLI and stores access token info.')
  // .option('-u, --username <username>', 'The user to authenticate as')
  // .option('-p, --password <password>', 'The user\'s password')
  .action(function() {

    
    var username = ( process.argv[3] ? process.argv[3] : null );
    var password = ( process.argv[4] ? process.argv[4] : null );

    console.log(". executing: config ");
    if(!username || !password) {
    console.log(". username and password are mandatory ");
    console.log("  usage: wt config <username> <password> ");
    } else {
    console.log(". validating creds");
    
    wtRequest.getToken(username,password, function(output) {
        
        if(output.error) {
        console.error(chalk.red(output.error));
        process.exit(1);
        };
        
        if(output.body) {
          //   var body = JSON.parse(output.body);
          console.log(output.body);
          settings.username=username;
          settings.accessToken=output.body.token;
          wtFiles.writeSettings(settings);
          console.log(". ended successfully");
        } else {
        console.log(". no token found");
        } 
    });
  }

});



program
  .command('activity <description>')
  .description('Records an activity reminder')
  .action(function() {

    var description = ( process.argv[3] ? process.argv[3] : null );
    
    //validating accessToken is set
    checkAccessToken();

    console.log(". executing: adding activity reminder ");
    
    //building timelog from params
    var data = {
      cmd: "activity",
      external_id: "",
      path: os.hostname(),
      activity : description
    };
    
    //sending request to API
    wtRequest.request(settings.accessToken,'activity', data, function(output) {
      if(output.error) {
        console.error(chalk.red(output.error));
        process.exit(1);
      } else {
        console.log(". ended successfully");
      };
      // console.log(output.body);
    });

  });

program
  .command('last')
  .description('records the ssh access logs entries as activities (uses the unix last command)')
  .action(function() {

    var description = ( process.argv[3] ? process.argv[3] : null );
    
    //validating accessToken is set
    checkAccessToken();

    //executes shell last command (for unix like sytems)
    var exec = require('child_process').exec;
    var cmd = 'last';

    console.log(". executing: last ssh access entries ");
    exec(cmd, function(error, stdout, stderr) {

      if(error) throw error;

      //building timelog from params
      var data = {
        cmd: "last",
        external_id: "",
        last_output: stdout,
        path: os.hostname(),
      };
      
      //sending request to API
      wtRequest.request(settings.accessToken,'last', data, function(output) {
        if(output.error) {
          console.error(chalk.red(output.error));
          process.exit(1);
        } else {
          console.log(". ended successfully");
        };
        // console.log(output.body);
      });
    });
    
    
    

  });   


program
  .command('start <description>')
  .description('Starts a timer')
  .option("-p, --project [projectId]", "Set projectId")
  .action(function(pDesc, options) {

    if(settings.timelogStartId) {
      console.log("ERROR: Timer already tracking time, please stop first.");
      return;
    }
    
    console.log(". executing: timelog start");
    // console.log(options);
    //validating accessToken is set
    checkAccessToken();

    //building timelog from params
    var data = {
      "hours" : 0,
      "activity" : pDesc,
      "type": "Regular",
      "project" : options.project ||  null ,
      "timerStart" : 0
    };
    
    //console.log(process.argv);
    
    //sending request to server. 
    wtRequest.request(settings.accessToken,'timelog', data, function(output) {
      if(output.error) {
        console.log(chalk.red(output.error));
        process.exit(1);
      };
      //console.log(output.body);
      settings.timelogStartId = output.body._id;
      wtFiles.writeSettings(settings);
      
      console.log(". ended successfully");
      

    });

  }
);

program
  .command('stop')
  .description('Stops timer and records timelog')
  .action(function() {

    console.log(". executing: timelog stop");
    // console.log(options);
    //validating accessToken is set
    checkAccessToken();

    //building timelog from params
    var data = {
      "id" : settings.timelogStartId
    };
    
    //console.log(process.argv);
    console.log('.. updating timelog ' + settings.timelogStartId);

    //sending request to server. 
    wtRequest.request(settings.accessToken,'timelog-stop', data, function(output) {
      if(output.error) {
        console.log(chalk.red(output.error));
        process.exit(1);
      };
      
      settings.timelogStartId = null;
      wtFiles.writeSettings(settings);
      
      console.log(". ended successfully");
    });

  }
);

program
  .command('timelog <hours> <description>')
  .description('Records timelog')
  .option("-p, --project [projectId]", "Set projectId")
  .action(function(pHours, pDesc, options) {

    console.log(". executing: timelog ");
    // console.log(options);
    //validating accessToken is set
    checkAccessToken();

    //building timelog from params
    var data = {
      "hours" : pHours,
      "activity" : pDesc,
      "type": "Regular",
      "project" : options.project ||  null ,
    };
    
    //console.log(process.argv);
    console.log('.. adding new timelog');

        //sending request to server. 
        wtRequest.request(settings.accessToken,'timelog', data, function(output) {
          if(output.error) {
            console.log(chalk.red(output.error));
            process.exit(1);
          };
          console.log(output.body);
          console.log(". ended successfully");
        });

  });

//parsing parameters
program.parse(process.argv);

//if no command is provided, then display help
if (!process.argv.slice(2).length) {
  program.help();
  console.log("\n");
};



